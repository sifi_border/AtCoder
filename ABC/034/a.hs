import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [x, y] <- map strToInt . words <$> getLine
    -- 出力
    putStrLn $ if (x < y) then "Better" else "Worse"

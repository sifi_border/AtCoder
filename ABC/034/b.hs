import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe

main :: IO ()
main = do
    -- 整数の入力
    n <- readLn
    -- 出力
    putStrLn . show $ if even n then n-1 else n+1

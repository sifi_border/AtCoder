#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define MAX 2048

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31;
const int MOD = 1000000007;

// SW(p, w);
typedef pair<double, double> saltyWater;

saltyWater SWs[MAX];

void solve(void){
    int N, K; scanf("%d%d\n", &N, &K);
    double low = 100, high = 0;
    rep(N, i){
        double w, p; scanf("%lf%lf\n", &w, &p);
        SWs[i] = saltyWater(p, w);
        low = min(p, low); high = max(p, high);
    }
    while(high - low > EPS){
        vector<double> salts;
        double mid = (high + low) / 2;
        rep(N, i) salts.push_back(((SWs[i].first)-mid)*(SWs[i].second)/100);
        sort(salts.rbegin(), salts.rend());
        if (accumulate(salts.begin(), salts.begin()+K, 0.0) >= 0) low = mid;
        else high = mid;
    }
    printf("%lf\n", (high + low) / 2);
}

int main(void){
  solve();
  return 0;
}

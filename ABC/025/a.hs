import Control.Applicative
import Control.Monad
import Data.List
import Data.Char

main :: IO ()
main = do
    -- 文字列の入力
    s <- sort . filter (`elem` ['a'..'z']) <$> getLine
    n <- readLn
    -- 出力
    putStrLn [s!!((n-1) `div` 5), s!!((n-1) `mod` 5)]

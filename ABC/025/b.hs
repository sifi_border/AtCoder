import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [n, low, high] <- map strToInt . words <$> getLine
    steps <- replicateM n $ do
        [p, q] <- words <$> getLine
        return (p, read q)
    -- 出力
    putStrLn $ let res = move low high steps in
            if res == 0 then show 0
            else if res > 0 then "East " ++ show res
                            else "West " ++ (show $ negate res)

step :: Int -> Int -> Int -> Int
step low high dis
    | dis < low = low
    | dis <= high = dis
    | otherwise = high

move :: Int -> Int -> [(String, Int)] -> Int
move low high steps = foldl (\acc (ew, dis) -> let s = step low high dis in if ew == "East" then acc + s else acc - s) 0 steps

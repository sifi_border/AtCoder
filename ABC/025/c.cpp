#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define MAX 200000

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31-1;
const int MOD = 1000000007;

int pointVer[2][3];
int pointHor[3][2];
int pointSum = 0;
//turn 1 -> 9 (when turn reaches 10, calculates final chokudai score)
int calcScore(int turn, vector<int> map[]){
    if (turn == 10){
        int chokudaiScore = 0;
        rep(2, i) rep(3, j) {
            if (map[i][j] == map[i+1][j]) chokudaiScore += pointVer[i][j];
        }
        rep(3, i) rep(2, j) {
            if (map[i][j] == map[i][j+1]) chokudaiScore += pointHor[i][j];
        }
        return chokudaiScore;
    }
    else {
        //act 0,1,2 <=> empty,chokudai,chokuko
        int act = ((turn%2)?1:2);
        int minscore = INF, maxscore = 0;
        rep(3, y) rep(3, x){
            if (map[y][x] == 0) {
                map[y][x] = act;
                int score = calcScore(turn+1, map);
                minscore = min(score, minscore);
                maxscore = max(score, maxscore);
                map[y][x] = 0;
            }
        }
        if (turn%2) return maxscore;
        else return minscore;
    }
}

//abc025c
void solve(void){
    //input
    rep(2, i) rep(3, j) {
        cin >> pointVer[i][j]; pointSum += pointVer[i][j];
    }
    rep(3, i) rep(2, j) {
        cin >> pointHor[i][j]; pointSum += pointHor[i][j];
    }
    //init
    vector<int> map[3];
    rep(3, i) rep(3, j) map[i].push_back(0);
    int chokudai = calcScore(1, map);
    int chokuko = pointSum - chokudai;
    printf("%d\n%d\n", chokudai, chokuko);
}

int main(void){
  solve();
  return 0;
}

import Control.Applicative
import Control.Monad

main :: IO ()
main = do
    -- 整数の入力
    a <- readLn
    -- 出力
    putStrLn . show $ (a `div` 2) ^ 2

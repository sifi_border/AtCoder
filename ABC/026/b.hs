import Control.Applicative
import Control.Monad
import Data.List
strToDouble s = (read :: String -> Double) s

main :: IO ()
main = do
    _ <- getLine
    radiums <- sort . map strToDouble . lines <$> getContents
    -- 出力
    putStrLn . show . abs . (* pi) $ foldl (\acc r -> negate $ r**2 + acc) 0 radiums

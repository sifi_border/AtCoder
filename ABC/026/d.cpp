#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31-1;
const int MOD = 1000000007;

void solve(void){
    int a, b, c; scanf("%d%d%d\n", &a, &b, &c);
    double low = 0, high = 128;
    while(high - low > EPS){
        double midt = (high+low) / 2;
        double ft = a*midt + b*sin(c*midt*PI);
        if (ft < 100) low = midt;
        else high = midt;
    }
    printf("%.16lf\n", (high+low) / 2);
}

int main(void){
  solve();
  return 0;
}

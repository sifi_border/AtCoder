#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define MAX 32

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31-1;
const int MOD = 1000000007;

typedef long long ll;

//buka(1-indexed)
vector<ll> bukaof[MAX];

ll calcSalary(int j){
    if (bukaof[j].empty()) return 1;
    ll salaryMin = INF, salaryMax = 0;
    rep (bukaof[j].size(), i) {
        salaryMin = min(salaryMin, calcSalary(bukaof[j][i]));
        salaryMax = max(salaryMax, calcSalary(bukaof[j][i]));
    }
    return salaryMin + salaryMax + 1;
}

void solve(void){
    int n; scanf("%d\n", &n);
    Rep(2, n, i){
        int d; scanf("%d\n", &d);
        bukaof[d].push_back(i);
    }
    printf("%lld\n", calcSalary(1));
}

int main(void){
  solve();
  return 0;
}

import Control.Applicative
import Control.Monad
import Data.List
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [_, t] <- map strToInt . words <$> getLine
    -- スペース区切り整数の入力
    customers <- map strToInt . lines <$> getContents
    -- 出力
    putStrLn . show $ openrange customers t

openrange :: [Int] -> Int -> Int
openrange [cus] t = t
openrange (cus1:cus2:rescus) t = (min t $ cus2 - cus1) + openrange (cus2:rescus) t

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define MAX 10001

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31-1;
const int MOD = 1000000007;

int roots[MAX][2];

void solve(void){
    int n, d, k;
    cin >> n >> d >> k;
    rep(d, i) scanf("%d %d\n", &roots[i][0], &roots[i][1]);
    rep(k, i) {
        int s, t; scanf("%d %d\n", &s, &t);
        int days = 0, tempCity = s;
        rep(d, j){
            days++;
            if (tempCity >= roots[j][0] and tempCity <= roots[j][1]) {
                if (t >= roots[j][0] and t <= roots[j][1]) {
                    printf("%d\n", j+1); break;
                }
                if (t >= tempCity) tempCity = roots[j][1];
                else tempCity = roots[j][0];
            }
        }
    }
}

int main(void){
  solve();
  return 0;
}

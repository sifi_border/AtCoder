import Control.Applicative
import Control.Monad
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [a, b, c, k] <- map strToInt . words <$> getLine
    [s, t] <- map strToInt . words <$> getLine
    let discount = if (s + t >= k) then c*(s+t) else 0
    -- 出力
    putStrLn . show $ a*s+b*t - discount

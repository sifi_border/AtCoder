import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [a, b, n] <- map strToInt . lines <$> getContents
    -- 出力
    putStrLn . show . head $ dropWhile (< n) [(lcm a b)*k | k <- [1..]]

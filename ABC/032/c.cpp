#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define MAX 200000

const double PI = 3.14159265358979323846;
const double EPS = 1e-12;
const int INF = 1<<31;
const int MOD = 1000000007;

int dx4[4]={1,0,-1,0};
int dy4[4]={0,1,0,-1};
int dx8[8]={1,0,-1,1,-1,1,0,-1};
int dy8[8]={1,1,1,0,0,-1,-1,-1};

typedef long long ll;
typedef pair<ll, ll> llP;
typedef pair<int, int> intP;
typedef std::priority_queue<int> IntPrioQueue; //Z->A
typedef std::priority_queue<int, std::vector<int>, std::greater<int> > IntReversePrioQueue; //A->Z

void solve(void){
    int n, k;
    cin >> n >> k;
    vector<int> numbers;
    bool zeroF = false;
    rep(n, i) {
        int num;
        scanf("%d\n", &num);
        if (num == 0) {
            zeroF = true; break;
        }
        if (num <= k) numbers.push_back(num);
    }
    if (zeroF){
        cout << n << '\n';
    }
    else if (numbers.empty()) {
        cout << 0 << '\n';
    }
    else {
        int m = numbers.size();
        ll tempPro = 1LL;
        int l = 0, r = 0, ans = 0;
        //answer range := [l, r)
        //when r reaches the end, the answer won't be longer
        while(r < m){
            if (tempPro * numbers[r] <= k) {
                tempPro *= numbers[r];
                r++;
            }
            else {
                tempPro /= numbers[l];
                l++;
            }
            /*printf("%d %d\n", l, r);*/
            ans = max(ans, r-l);
        }
        cout << ans << endl;
    }
}

int main(void){
  solve();
  return 0;
}

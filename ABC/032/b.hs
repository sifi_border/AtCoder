import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe
strToInt s = (read :: String -> Int) s
strToInteger s = (read :: String -> Integer) s
strToDouble s = (read :: String -> Double) s

main :: IO ()
main = do
    s <- filter (`elem` ['a'..'z']) <$> getLine
    k <- readLn
    -- 出力
    putStrLn . show . length . group . sort $ substrings k s

substrings :: Int -> String -> [String]
substrings k s = if (length s < k) then [] else take k s : substrings k (tail s)

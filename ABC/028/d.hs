import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe
import Text.Printf

main :: IO ()
main = do
    -- スペース区切り整数の入力
    [n, k] <- map (read :: String -> Double ) . words <$> getLine
    printf "%.16f\n" $ (6*(k-1)*(n-k)+3*(n-1)+1) / n**3

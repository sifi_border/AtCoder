import Control.Applicative
import Control.Monad
import Data.List
import Data.Char
import Data.Maybe
strToInt s = (read :: String -> Int) s

main :: IO ()
main = do
    -- 整数の入力
    n <- readLn
    putStrLn $ evaluate n

evaluate :: Int -> String
evaluate n
    | n <= 59 = "Bad"
    | n <= 89 = "Good"
    | n <= 99 = "Great"
    | n == 100 = "Perfect"
    | otherwise = undefined

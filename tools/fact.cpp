#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace std;

#define Rep(b, e, i) for(int i = b; i <= e; i++)
#define rep(n, i) Rep(0, n-1, i)
#define INF 10000000
#define MOD (int)1e9+7
#define MAX 100000

typedef long long ll;

//階乗とその逆元
ll fac[MAX+1],facInv[MAX+1];

ll power(ll e, ll x){ //e^x % MOD
    if (x == 0) return 1LL;
    if (x % 2 != 0) return ((power(e, x-1) * e) % MOD);
    ll temp = power(e, x / 2);
    return (temp * temp) % MOD;
}

ll nck(ll n, ll k){
    if (!(n >= k && k >= 0)) return 0;
    ll temp = (fac[n] * facInv[n-k]) % MOD;
    return ((temp * facInv[k]) % MOD);
}

void fact(void){
    //階乗とその逆元
    fac[0] = facInv[0] = 1; //0! = 1
    //(x!)^(-1) ≡ (x!)^(p-2) (mod p)
    Rep(1, MAX, i) fac[i] = (fac[i-1] * i) % MOD;
    facInv[MAX] = power(fac[MAX], MOD-2);
    Rep(1, MAX-1, i) facInv[MAX-i] = (facInv[MAX-i+1] * (MAX-i+1)) % MOD;
}

int main(void){
  fact();
  return 0;
}
